package webServices;

import java.sql.*;
import library.User;

public class RegistrationDAO {
	
	public RegistrationDAO(){		
		try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
	}
	static Connection con=null;
	public static int register(User u){
		int status=0;
		try{
			
			String connectionURL = "jdbc:mysql://localhost:3306/library";
			
			Class.forName("com.mysql.jdbc.Driver"); 
			
			con = DriverManager.getConnection(connectionURL, "root", "");
			
            PreparedStatement ps=con.prepareStatement("insert into users(name, password, email, zipcode, city, street, houseNr, floorAndDoor, urank) values(?,?,?,?,?,?,?,?,?)");
			ps.setString(1,u.getName());
			ps.setString(2,u.getPassword());
			ps.setString(3,u.getEmail());
			ps.setInt(4,u.getZipcode());
			ps.setString(5,u.getCity());
			ps.setString(6,u.getStreet());
			ps.setString(7,u.getHouseNr());
			ps.setString(8,u.getFloorAndDoor());
			ps.setInt(9,u.getRank());
									
			status = ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return status;
	}
}
