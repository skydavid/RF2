package library;


public class User {
	private String name;
	private String password;
	private String email;
	private int zipcode;
	private String city;
	private String street;
	private String houseNr;
	private String floorAndDoor="";
	private int rank=1;
	public User(){
		floorAndDoor="";
		rank=1;
	}
	public User(User u){
		this.name = u.name;
		this.password = u.password;
		this.email = u.email;
		this.zipcode = u.zipcode;
		this.city = u.city;
		this.street = u.street;
		this.houseNr = u.houseNr;
		this.floorAndDoor = u.floorAndDoor;
		this.rank=1;
		
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHouseNr() {
		return houseNr;
	}
	public void setHouseNr(String houseNr) {
		this.houseNr = houseNr;
	}
	public String getFloorAndDoor() {
		return floorAndDoor;
	}
	public void setFloorAndDoor(String floorAndDoor) {
		this.floorAndDoor = floorAndDoor;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}

}
