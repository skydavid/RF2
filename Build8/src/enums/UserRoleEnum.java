package enums;

public enum UserRoleEnum {
	USER(1),
	LIBRARIAN(2),
	ADMIN(3);
	
	private final int rank;
	
	private UserRoleEnum(final int rank) {
		this.rank = rank;
	}
	
    public int getValue() {
        return rank;
    }
}
