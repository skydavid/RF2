﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


CREATE TABLE books (
id INT NOT NULL AUTO_INCREMENT,
imei VARCHAR(20) NOT NULL,
author VARCHAR(30) NOT NULL,
title VARCHAR(50) NOT NULL,
piece INT,
availablePieces INT,
numberOfLoanings INT,
imageurl VARCHAR(30),
publisherName VARCHAR(20),
publishingDate INT,
description VARCHAR(200),
genre VARCHAR(50),

PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

CREATE TABLE users (
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(40) NOT NULL,
password VARCHAR(20) NOT NULL,
email VARCHAR(20) NOT NULL,
zipcode INT,
city VARCHAR(20),
street VARCHAR(30),
houseNr VARCHAR(6),
floorAndDoor VARCHAR(20),
urank INT,

PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

CREATE TABLE lentBooks (
id INT NOT NULL AUTO_INCREMENT,
bookid INT,
userid INT,
startDate DATE,
deadlineDate DATE,
returndate DATE,

PRIMARY KEY(id),
FOREIGN KEY (userid) REFERENCES users(id) ON UPDATE CASCADE ON DELETE SET NULL,
FOREIGN KEY (bookid) REFERENCES books(id) ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

CREATE TABLE comments (
id INT NOT NULL AUTO_INCREMENT,
ccomment VARCHAR(100),
cdate DATE,
userid INT,
bookid INT,

PRIMARY KEY(id),
FOREIGN KEY (userid) REFERENCES users(id) ON UPDATE CASCADE ON DELETE SET NULL,
FOREIGN KEY (bookid) REFERENCES books(id) ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;



INSERT INTO books VALUES(0, '607579094290157', 'G. Hajnóczy Rózsa', 'A bengáli tűz', 5, 5, 0, NULL, 'Alexandra', 1985, 'nincs', 'horror');
INSERT INTO books VALUES(1, '122288614557368', 'Mikszáth Kálmán', 'A fekete város', 6, 6, 0, NULL, 'Akadémiai Kiadó', 2000, 'nincs', 'thriller');
INSERT INTO books VALUES(2, '461667316374763', 'Füst Milán', 'A feleségem története', 7, 7, 0, NULL, 'Panem Kiadó', 2000, 'nincs', 'kotelezo');
INSERT INTO books VALUES(3, '690839528068075', 'Wass Albert', 'A funtineli boszorkány', 3, 3, 0, NULL, 'Readers Digest', 2004, 'nincs', 'thriller');
INSERT INTO books VALUES(4, '26602970376624', 'Babits Mihály', 'A gólyakalifa', 1, 1, 0, NULL, 'Alexandra', 2000, 'nincs', 'nincs');
INSERT INTO books VALUES(5, '538224881608123', 'Márai Sándor', 'A gyertyák csonkig égnek', 4, 4, 0, NULL, 'Akadémiai Kiadó', 2000, 'nincs', 'scifi');
INSERT INTO books VALUES(6, '506791800787771', 'Rejtő Jenő', 'A három testőr Afrikában', 3, 3, 0, NULL, 'Panem Kiadó', 2000, 'nincs', 'horror');
INSERT INTO books VALUES(7, '230479796306599', 'Lőrincz L. László', 'A kicsik', 2, 2, 0, NULL, 'Readers Digest', 2000, 'nincs', 'scifi');
INSERT INTO books VALUES(8, '40365222254828', 'Jókai Mór', 'A kőszívű ember fiai', 1, 1, 0, NULL, 'Alexandra', 1998, 'nincs', 'kotelezo');
INSERT INTO books VALUES(9, '938275557271994', 'Gárdonyi Géza', 'A láthatatlan ember', 5, 5, 0, NULL, 'Akadémiai Kiadó', 2002, 'nincs', 'horror');
INSERT INTO books VALUES(10, '130083021846415', 'Dallos Sándor', 'A nap szerelmese', 4, 4, 0, NULL, 'Alexandra', 2000, 'nincs', 'scifi');
INSERT INTO books VALUES(11, '370907789448223', 'Molnár Ferenc', 'A Pál utcai fiúk', 3, 3, 0, NULL, 'Panem Kiadó', 1990, 'nincs', 'horror');
INSERT INTO books VALUES(12, '156736940445982', 'Szerb Antal', 'A Pendragon legenda', 7, 7, 0, NULL, 'Readers Digest', 2000, 'nincs', 'scifi');
INSERT INTO books VALUES(13, '475555120883442', 'Rejtő Jenő', 'A tizennégy karátos autó', 6, 6, 0, NULL, 'Alexandra', 2000, 'nincs', 'horror');
INSERT INTO books VALUES(14, '819078062018416', 'Szepes Mária', 'A vörös oroszlán', 3, 3, 0, NULL, 'Readers Digest', 2000, 'nincs', 'thriller');
INSERT INTO books VALUES(15, '941805463385116', 'Tamási Áron', 'Ábel a rengetegben', 2, 2, 0, NULL, 'Alexandra', 2000, 'nincs', 'thriller');
INSERT INTO books VALUES(16, '603826689624339', 'Wass Albert', 'Adjátok vissza a hegyeimet!', 1, 1, 0, NULL, 'Panem Kiadó', 2000, 'nincs', 'scifi');
INSERT INTO books VALUES(17, '751326599052358', 'Rácz Zsuzsa', 'Állítsátok meg Terézanyut!', 4, 4, 0, NULL, 'Readers Digest', 2000, 'nincs', 'kotelezo');
INSERT INTO books VALUES(18, '83979013596615', 'Vámos Miklós', 'Apák könyve', 3, 3, 0, NULL, 'Akadémiai Kiadó', 2000, 'nincs', 'scifi');
INSERT INTO books VALUES(19, '823121199946089', 'Dallos Sándor', 'Aranyecset', 2, 2, 0, NULL, 'Panem Kiadó', 2001, 'nincs', 'kotelezo');



INSERT INTO users VALUES(0, 'Hullár Tamás', 'asd', 'xvid95@gmail.com', 6421, 'Kisszállás', 'Otthon u.', 34, '', 1);
INSERT INTO users VALUES(1, 'Nagy Bence', 'lib', 'lib@lib.com', 3554, 'Bükkaranyos', 'Hajó u.', 51, '3/8', 2);
INSERT INTO users VALUES(2, 'Péter Anna', 'jelszo', 'Péter.Anna@gmail.com', 9547, 'Karakó', 'Bem József u.', 23, '', 1);
INSERT INTO users VALUES(3, 'admin', 'admin', 'admin@admin.com', 8447, 'Ajka', 'Nefelejcs u.', 3, '', 3);
INSERT INTO users VALUES(4, 'Schäfer Andrea', '12345', 'Schäfer.Andrea@gmail.com', 3511, 'Miskolc', 'Vöröshajnal út', 75, '', 1);
INSERT INTO users VALUES(5, 'Fehér Zoltán', '111111', 'Fehér.Zoltán@gmail.com', 9985, 'Felsőszölnök', 'Harmadik u.', 63, '', 1);
INSERT INTO users VALUES(6, 'Novák Péter', 'novakpeter', 'Novák.Péter@gmail.com', 4320, 'Nagykálló', 'Flórián út', 24, '', 1);
INSERT INTO users VALUES(7, 'Kiss Roland', 'krpw', 'Kiss.Roland@gmail.com', 2064, 'Bicske', 'Esze Tamás u.', 70, '6/16', 1);
INSERT INTO users VALUES(8, 'Krammer Martin', '111111', 'Krammer.Martin@gmail.com', 6031, 'Szentkirály', 'Szövő u.', 88, '', 1);
INSERT INTO users VALUES(9, 'Sétáló Szimóna', '66625', 'Sétáló.Szimóna@gmail.com', 8734, 'Somogyzsitfa', 'Csutak Kálmán u.', 25, '', 1);
INSERT INTO users VALUES(10, 'Pass Nikolett', '111111', 'Pass.Nikolett@gmail.com', 4262, 'Nyíracsád', 'Kiss Ernő u.', 75, '', 1);
INSERT INTO users VALUES(11, 'Csubák Ádám', '111111', 'Csubák.Ádám@gmail.com', 6785, 'Pusztamérges', 'Tavasz u.', 66, '', 1);
INSERT INTO users VALUES(12, 'Szabó Bettina', '111111', 'Szabó.Bettina@gmail.com', 8738, 'Nemesvid', 'Szövő sétány', 26, '', 1);
INSERT INTO users VALUES(13, 'Komlós Krisztina', '111111', 'Komlós.Krisztina@gmail.com', 6754, 'Újszentiván', 'István király u.', 49, '', 1);
INSERT INTO users VALUES(14, 'Kupsza Réka', '111111', 'Kupsza.Réka@gmail.com', 2532, 'Tokodaltáró', 'Kiss Ernő u.', 29, '', 1);
INSERT INTO users VALUES(15, 'Lökös Beatrix', '111111', 'Lökös.Beatrix@gmail.com', 5061, 'Tiszasüly', 'Sorház u.', 87, '', 1);
INSERT INTO users VALUES(16, 'Tábori Nikolett', '111111', 'Tábori.Nikolett@gmail.com', 8724, 'Inke', 'Tél u.', 67, '', 1);
INSERT INTO users VALUES(17, 'Szujer Lajos', '111111', 'Szujer.Lajos@gmail.com', 7304, 'Mánfa', 'Vörössipkás út', 66, '', 1);
INSERT INTO users VALUES(18, 'Kocsis Benjámin', '111111', 'Kocsis.Benjámin@gmail.com', 4536, 'Ramocsaháza', 'Juhar u.', 56, '', 1);
INSERT INTO users VALUES(19, 'Tóth Bettina', '111111', 'Tóth.Bettina@gmail.com', 8242, 'Balatonudvari', 'Arany János u.', 95, '', 1);
INSERT INTO users VALUES(20, 'Bölcskei Sándor', '111111', 'Bölcskei.Sándor@gmail.com', 2677, 'Herencsény', 'Szabó Ervin u.', 82, '', 1);
INSERT INTO users VALUES(21, 'Erdei Viktória', '111111', 'Erdei.Viktória@gmail.com', 8143, 'Sárszentmihály', 'Szárcsa krt.', 96, '', 1);
INSERT INTO users VALUES(22, 'Gál Ibolya', '111111', 'Gál.Ibolya@gmail.com', 7381, 'Ág', 'Traktoros u.', 55, '', 1);
INSERT INTO users VALUES(23, 'Bíró Judit', '111111', 'Bíró.Judit@gmail.com', 9123, 'Kajárpéc', 'Munkás u.', 18, '', 1);
INSERT INTO users VALUES(24, 'Huber Attila', '111111', 'Huber.Attila@gmail.com', 8973, 'Alsószenterzsébet', 'Váci Mihály u.', 99, '', 1);