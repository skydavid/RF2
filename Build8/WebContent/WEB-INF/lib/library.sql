-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Hoszt: 127.0.0.1
-- Létrehozás ideje: 2013. Nov 25. 22:12
-- Szerver verzió: 5.5.32
-- PHP verzió: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `library`
--
CREATE DATABASE IF NOT EXISTS `library` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `library`;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `author` text NOT NULL,
  `imei` text NOT NULL,
  `publishercity` text NOT NULL,
  `publishername` text NOT NULL,
  `imageurl` text NOT NULL,
  `piece` int(11) NOT NULL,
  `outpiece` int(11) NOT NULL,
  `publishdate` int(11) NOT NULL,
  `tag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- A tábla adatainak kiíratása `books`
--

INSERT INTO `books` (`id`, `name`, `author`, `imei`, `publishercity`, `publishername`, `imageurl`, `piece`, `outpiece`, `publishdate`, `tag`) VALUES
(1, 'A bengáli t?z', 'G. Hajnóczy Rózsa', '607579094290157', 'Budapest', 'Alexandra', 'nincs', 5, 0, 1985, 1),
(2, 'A fekete város', 'Mikszáth Kálmán', '122288614557368', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2000, 1),
(3, 'A feleségem története', 'Füst Milán', '461667316374763', 'Budapest', 'Panem Kiadó', 'nincs', 1, 0, 2000, 1),
(4, 'A funtineli boszorkány', 'Wass Albert', '690839528068075', 'Budapest', 'Readers Digest', 'nincs', 6, 0, 2004, 1),
(5, 'A gólyakalifa', 'Babits Mihály', '26602970376624', 'Budapest', 'Alexandra', 'nincs', 2, 0, 2000, 1),
(6, 'A gyertyák csonkig égnek', 'Márai Sándor', '538224881608123', 'Budapest', 'Akadémiai Kiadó', 'nincs', 13, 0, 2000, 1),
(7, 'A három test?r Afrikában', 'Rejt? Jen?', '506791800787771', 'Budapest', 'Panem Kiadó', 'nincs', 21, 0, 2000, 1),
(8, 'A kicsik', 'L?rincz L. László', '230479796306599', 'Budapest', 'Readers Digest', 'nincs', 10, 0, 2000, 1),
(9, 'A k?szív? ember fiai', 'Jókai Mór', '40365222254828', 'Budapest', 'Alexandra', 'nincs', 2, 0, 2000, 1),
(10, 'A láthatatlan ember', 'Gárdonyi Géza', '938275557271994', 'Budapest', 'Akadémiai Kiadó', 'nincs', 5, 0, 2002, 1),
(11, 'A nap szerelmese', 'Dallos Sándor', '130083021846415', 'Budapest', 'Alexandra', 'nincs', 2, 0, 2000, 1),
(12, 'A Négyszöglet? Kerek Erd?', 'Lázár Ervin', '99558985886,4593', 'Budapest', 'Akadémiai Kiadó', 'nincs', 1, 0, 2000, 1),
(13, 'A Pál utcai fiúk', 'Molnár Ferenc', '370907789448223', 'Budapest', 'Panem Kiadó', 'nincs', 6, 0, 2002, 1),
(14, 'A Pendragon legenda', 'Szerb Antal', '156736940445982', 'Budapest', 'Readers Digest', 'nincs', 2, 0, 2000, 1),
(15, 'A tizennégy karátos autó', 'Rejt? Jen?', '475555120883442', 'Budapest', 'Alexandra', 'nincs', 13, 0, 2000, 1),
(16, 'A vörös oroszlán', 'Szepes Mária', '819078062018416', 'Budapest', 'Readers Digest', 'nincs', 21, 0, 2000, 1),
(17, 'Ábel a rengetegben', 'Tamási Áron', '941805463385116', 'Budapest', 'Alexandra', 'nincs', 10, 0, 2000, 1),
(18, 'Abigél', 'Szabó Magda', '2144093171,5339', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2000, 1),
(19, 'Adjátok vissza a hegyeimet!', 'Wass Albert', '603826689624339', 'Budapest', 'Panem Kiadó', 'nincs', 5, 0, 2000, 1),
(20, 'Állítsátok meg Terézanyut!', 'Rácz Zsuzsa', '751326599052358', 'Budapest', 'Readers Digest', 'nincs', 2, 0, 2000, 1),
(21, 'Anya csak egy van', 'Vámos Miklós', '37233575642,1677', 'Budapest', 'Alexandra', 'nincs', 1, 0, 2000, 1),
(22, 'Apák könyve', 'Vámos Miklós', '83979013596615', 'Budapest', 'Akadémiai Kiadó', 'nincs', 6, 0, 2000, 1),
(23, 'Aranyecset', 'Dallos Sándor', '823121199946089', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2000, 1),
(24, 'Aranykoporsó', 'Móra Ferenc', '341615069247442', 'Budapest', 'Readers Digest', 'nincs', 13, 0, 2000, 1),
(25, 'Árvácska', 'Móricz Zsigmond', '921335705599197', 'Budapest', 'Alexandra', 'nincs', 21, 0, 2000, 1),
(26, 'Az ajtó', 'Szabó Magda', '327538596244715', 'Budapest', 'Alexandra', 'nincs', 10, 0, 2000, 1),
(27, 'Az arany ember', 'Jókai Mór', '430478416842089', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2000, 1),
(28, 'Az ötödik pecsét', 'Sánta Ferenc', '604356851883606', 'Budapest', 'Panem Kiadó', 'nincs', 10, 0, 2000, 1),
(29, 'Beszterce ostroma', 'Mikszáth Kálmán', '381997717382717', 'Budapest', 'Readers Digest', 'nincs', 5, 0, 2000, 1),
(30, 'Bezzeg az én id?mben', 'Fehér Klára', '671159704784144', 'Budapest', 'Alexandra', 'nincs', 2, 0, 2000, 1),
(31, 'Bogáncs', 'Fekete István', '10573816777825', 'Budapest', 'Panem Kiadó', 'nincs', 1, 0, 2000, 1),
(32, 'Csontbrigád', 'Rejt? Jen?', '485079398978375', 'Budapest', 'Readers Digest', 'nincs', 6, 0, 2000, 1),
(33, 'Édes Anna', 'Kosztolányi Dezs?', '63825864118084', 'Budapest', 'Alexandra', 'nincs', 2, 0, 2000, 1),
(34, 'Éget? Eszter', 'Németh László', '269435950586106', 'Budapest', 'Alexandra', 'nincs', 13, 0, 2000, 1),
(35, 'Egri csillagok', 'Gárdonyi Géza', '623050444451662', 'Budapest', 'Akadémiai Kiadó', 'nincs', 21, 0, 2000, 1),
(36, 'Egy magyar nábob', 'Jókai Mór', '337091978607932', 'Budapest', 'Panem Kiadó', 'nincs', 10, 0, 2000, 1),
(37, 'Egy polgár vallomásai', 'Márai Sándor', '196531497037925', 'Budapest', 'Readers Digest', 'nincs', 2, 0, 2000, 1),
(38, 'Egymás szemében', 'Szilvási Lajos', '30862931361118', 'Budapest', 'Alexandra', 'nincs', 10, 0, 2000, 1),
(39, 'Elvész a nyom', 'Wass Albert', '687014588389764', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2010, 1),
(40, 'Emlékiratok könyve', 'Nádas Péter', '704650892240999', 'Budapest', 'Panem Kiadó', 'nincs', 5, 0, 1980, 1),
(41, 'Erdély', 'Móricz Zsigmond', '24072771204945', 'Budapest', 'Readers Digest', 'nincs', 2, 0, 1985, 1),
(42, 'Es?isten siratja Mexikót', 'Passuth László', '884635517538104', 'Budapest', 'Alexandra', 'nincs', 1, 0, 2000, 1),
(43, 'Fekete gyémántok', 'Jókai Mór', '43539297319217', 'Budapest', 'Akadémiai Kiadó', 'nincs', 6, 0, 2000, 1),
(44, 'Für Elise', 'Szabó Magda', '33836947240671', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2000, 1),
(45, 'Gerg? és az álomfogók', 'Böszörményi Gyula', '144118257069394', 'Budapest', 'Panem Kiadó', 'nincs', 13, 0, 2001, 1),
(46, 'Harmonia caelestis', 'Esterházy Péter', '243752391531498', 'Budapest', 'Readers Digest', 'nincs', 21, 0, 2000, 1),
(47, 'Ida regénye', 'Gárdonyi Géza', '474423483693187', 'Budapest', 'Alexandra', 'nincs', 10, 0, 2002, 1),
(48, 'Indul a bakterház', 'Rideg Sándor', '540768972787095', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2000, 1),
(49, 'Iskola a határon', 'Ottlik Géza', '336578573859469', 'Budapest', 'Panem Kiadó', 'nincs', 10, 0, 2000, 1),
(50, 'Isten rabjai', 'Gárdonyi Géza', '460484524100684', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2006, 1),
(51, 'Iszony', 'Németh László', '794478310541365', 'Budapest', 'Panem Kiadó', 'nincs', 5, 0, 2000, 1),
(52, 'Jadviga párnája', 'Závada Pál', '444989277278183', 'Budapest', 'Readers Digest', 'nincs', 2, 0, 2010, 1),
(53, 'Kard és kasza', 'Wass Albert', '893689858078704', 'Budapest', 'Panem Kiadó', 'nincs', 1, 0, 1980, 1),
(54, 'Karnevál', 'Hamvas Béla', '493537275855057', 'Budapest', 'Readers Digest', 'nincs', 6, 0, 1985, 1),
(55, 'Kincskeres? kisködmön', 'Móra Ferenc', '710002374652512', 'Budapest', 'Alexandra', 'nincs', 45, 0, 2000, 1),
(56, 'Különös házasság', 'Mikszáth Kálmán', '887327894314306', 'Budapest', 'Akadémiai Kiadó', 'nincs', 13, 0, 2000, 1),
(57, 'Légy jó mindhalálig', 'Móricz Zsigmond', '53263012117698', 'Budapest', 'Readers Digest', 'nincs', 5, 0, 2004, 1),
(58, 'Mesemaraton', 'Vavyan Fable', '878071436128974', 'Budapest', 'Alexandra', 'nincs', 2, 0, 2000, 1),
(59, 'Ne féljetek', 'Jókai Anna', '183742659783605', 'Budapest', 'Alexandra', 'nincs', 1, 0, 1985, 1),
(60, 'Pacsirta', 'Kosztolányi Dezs?', '81733036373446', 'Budapest', 'Akadémiai Kiadó', 'nincs', 66, 0, 2000, 1),
(61, 'Pete Pite', 'Nógrádi Gábor', '975545420911011', 'Budapest', 'Readers Digest', 'nincs', 2, 0, 2000, 1),
(62, 'Piszkos Fred, a kapitány', 'Rejt? Jen?', '68341089550893', 'Budapest', 'Alexandra', 'nincs', 13, 0, 2004, 1),
(63, 'Pokolbéli víg napjaim', 'Faludy György', '91373361165,0049', 'Budapest', 'Alexandra', 'nincs', 21, 0, 2000, 1),
(64, 'Régimódi történet', 'Szabó Magda', '470575954571508', 'Budapest', 'Akadémiai Kiadó', 'nincs', 10, 0, 2000, 1),
(65, 'Rokonok', 'Móricz Zsigmond', '830397775041761', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2000, 1),
(66, 'Sorstalanság', 'Kertész Imre', '891911853202859', 'Budapest', 'Readers Digest', 'nincs', 2, 0, 2000, 1),
(67, 'Szent Péter eserny?je', 'Mikszáth Kálmán', '634897454136104', 'Budapest', 'Alexandra', 'nincs', 1, 0, 2000, 1),
(68, 'Téli berek', 'Fekete István', '340160481681991', 'Budapest', 'Panem Kiadó', 'nincs', 6, 0, 2002, 1),
(69, 'Titkok egy régi kertben', 'Kálnay Adél', '301274665879222', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2002, 1),
(70, 'Tóték', 'Örkény István', '250481072139486', 'Budapest', 'Readers Digest', 'nincs', 13, 0, 2000, 1),
(71, 'Tüskevár', 'Fekete István', '546274007095956', 'Budapest', 'Alexandra', 'nincs', 21, 0, 1985, 1),
(72, 'Utas és holdvilág', 'Szerb Antal', '717286213025103', 'Budapest', 'Akadémiai Kiadó', 'nincs', 10, 0, 2000, 1),
(73, 'Utazás a koponyám körül', 'Karinthy Frigyes', '178715546585399', 'Budapest', 'Panem Kiadó', 'nincs', 5, 0, 2000, 1),
(74, 'Vidravas', 'Galgóczi Erzsébet', '730202151799123', 'Budapest', 'Panem Kiadó', 'nincs', 60, 0, 2004, 1),
(75, 'A bengáli t?z', 'G. Hajnóczy Rózsa', '702617708601675', 'Budapest', 'Readers Digest', 'nincs', 1, 0, 2000, 1),
(76, 'A fekete város', 'Mikszáth Kálmán', '120025665913988', 'Budapest', 'Alexandra', 'nincs', 6, 0, 2000, 1),
(77, 'A feleségem története', 'Füst Milán', '46917444538297', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2000, 1),
(78, 'A funtineli boszorkány', 'Wass Albert', '615719839452574', 'Budapest', 'Panem Kiadó', 'nincs', 13, 0, 2000, 1),
(79, 'A gólyakalifa', 'Babits Mihály', '154388486420986', 'Budapest', 'Akadémiai Kiadó', 'nincs', 21, 0, 2000, 1),
(80, 'A gyertyák csonkig égnek', 'Márai Sándor', '654889845894058', 'Budapest', 'Panem Kiadó', 'nincs', 35, 0, 2002, 1),
(81, 'A három test?r Afrikában', 'Rejt? Jen?', '690601396267787', 'Budapest', 'Readers Digest', 'nincs', 5, 0, 2000, 1),
(82, 'A kicsik', 'L?rincz L. László', '71577305677182', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2000, 1),
(83, 'A k?szív? ember fiai', 'Jókai Mór', '606390478921967', 'Budapest', 'Panem Kiadó', 'nincs', 1, 0, 2000, 1),
(84, 'A láthatatlan ember', 'Gárdonyi Géza', '570723154955395', 'Budapest', 'Panem Kiadó', 'nincs', 6, 0, 2000, 1),
(85, 'A nap szerelmese', 'Dallos Sándor', '569975072626279', 'Budapest', 'Readers Digest', 'nincs', 2, 0, 1985, 1),
(86, 'A Négyszöglet? Kerek Erd?', 'Lázár Ervin', '101476209242054', 'Budapest', 'Alexandra', 'nincs', 13, 0, 2000, 1),
(87, 'A Pál utcai fiúk', 'Molnár Ferenc', '719424193544288', 'Budapest', 'Akadémiai Kiadó', 'nincs', 21, 0, 2000, 1),
(88, 'A Pendragon legenda', 'Szerb Antal', '669206990455489', 'Budapest', 'Panem Kiadó', 'nincs', 10, 0, 2004, 1),
(89, 'A tizennégy karátos autó', 'Rejt? Jen?', '72423069985198', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2000, 1),
(90, 'A vörös oroszlán', 'Szepes Mária', '16572785518995', 'Budapest', 'Readers Digest', 'nincs', 13, 0, 2000, 1),
(91, 'Ábel a rengetegben', 'Tamási Áron', '811379115091801', 'Budapest', 'Alexandra', 'nincs', 21, 0, 2000, 1),
(92, 'Abigél', 'Szabó Magda', '513343724814135', 'Budapest', 'Akadémiai Kiadó', 'nincs', 10, 0, 2000, 1),
(93, 'Adjátok vissza a hegyeimet!', 'Wass Albert', '878458716330146', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2000, 1),
(94, 'Állítsátok meg Terézanyut!', 'Rácz Zsuzsa', '863355601414416', 'Budapest', 'Akadémiai Kiadó', 'nincs', 5, 0, 2002, 1),
(95, 'Anya csak egy van', 'Vámos Miklós', '589635210272827', 'Budapest', 'Readers Digest', 'nincs', 2, 0, 2000, 1),
(96, 'Apák könyve', 'Vámos Miklós', '75282145781109', 'Budapest', 'Alexandra', 'nincs', 1, 0, 2000, 1),
(97, 'Aranyecset', 'Dallos Sándor', '44899548039009', 'Budapest', 'Alexandra', 'nincs', 6, 0, 2006, 1),
(98, 'Aranykoporsó', 'Móra Ferenc', '174353056635775', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2000, 1),
(99, 'Árvácska', 'Móricz Zsigmond', '157461308281398', 'Budapest', 'Panem Kiadó', 'nincs', 13, 0, 2010, 1),
(100, 'Az ajtó', 'Szabó Magda', '721380635051615', 'Budapest', 'Readers Digest', 'nincs', 21, 0, 1980, 1),
(101, 'Az arany ember', 'Jókai Mór', '563894714523807', 'Budapest', 'Alexandra', 'nincs', 35, 0, 1985, 1),
(102, 'Az ötödik pecsét', 'Sánta Ferenc', '360093519631505', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2000, 1),
(103, 'Beszterce ostroma', 'Mikszáth Kálmán', '261363031516718', 'Budapest', 'Panem Kiadó', 'nincs', 13, 0, 2000, 1),
(104, 'Bezzeg az én id?mben', 'Fehér Klára', '721700885825039', 'Budapest', 'Readers Digest', 'nincs', 21, 0, 2004, 1),
(105, 'Bogáncs', 'Fekete István', '230894045818513', 'Budapest', 'Alexandra', 'nincs', 10, 0, 2000, 1),
(106, 'Csontbrigád', 'Rejt? Jen?', '816022340893975', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2000, 1),
(107, 'Édes Anna', 'Kosztolányi Dezs?', '224270298601666', 'Budapest', 'Panem Kiadó', 'nincs', 20, 0, 2000, 1),
(108, 'Éget? Eszter', 'Németh László', '259122942565504', 'Budapest', 'Panem Kiadó', 'nincs', 1, 0, 2000, 1),
(109, 'Egri csillagok', 'Gárdonyi Géza', '254836549883098', 'Budapest', 'Readers Digest', 'nincs', 6, 0, 2000, 1),
(110, 'Egy magyar nábob', 'Jókai Mór', '550669211096821', 'Budapest', 'Alexandra', 'nincs', 2, 0, 2002, 1),
(111, 'Egy polgár vallomásai', 'Márai Sándor', '612680862420058', 'Budapest', 'Akadémiai Kiadó', 'nincs', 13, 0, 2000, 1),
(112, 'Egymás szemében', 'Szilvási Lajos', '706994587365621', 'Budapest', 'Panem Kiadó', 'nincs', 21, 0, 2000, 1),
(113, 'Elvész a nyom', 'Wass Albert', '166217399624462', 'Budapest', 'Akadémiai Kiadó', 'nincs', 10, 0, 2006, 1),
(114, 'Emlékiratok könyve', 'Nádas Péter', '29657200336536', 'Budapest', 'Readers Digest', 'nincs', 2, 0, 2000, 1),
(115, 'Erdély', 'Móricz Zsigmond', '474820566842827', 'Budapest', 'Alexandra', 'nincs', 13, 0, 2010, 1),
(116, 'Es?isten siratja Mexikót', 'Passuth László', '510597515555258', 'Budapest', 'Alexandra', 'nincs', 35, 0, 1980, 1),
(117, 'Fekete gyémántok', 'Jókai Mór', '476605867267972', 'Budapest', 'Akadémiai Kiadó', 'nincs', 10, 0, 1985, 1),
(118, 'Für Elise', 'Szabó Magda', '94880322899,3652', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2000, 1),
(119, 'Gerg? és az álomfogók', 'Böszörményi Gyula', '798777926639086', 'Budapest', 'Readers Digest', 'nincs', 1, 0, 2000, 1),
(120, 'Harmonia caelestis', 'Esterházy Péter', '589892331706439', 'Budapest', 'Alexandra', 'nincs', 6, 0, 2000, 1),
(121, 'Ida regénye', 'Gárdonyi Géza', '7773669571552', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2001, 1),
(122, 'Indul a bakterház', 'Rideg Sándor', '919790138801941', 'Budapest', 'Panem Kiadó', 'nincs', 13, 0, 2000, 1),
(123, 'Iskola a határon', 'Ottlik Géza', '28920913702633', 'Budapest', 'Readers Digest', 'nincs', 21, 0, 2002, 1),
(124, 'Isten rabjai', 'Gárdonyi Géza', '36395326546865', 'Budapest', 'Alexandra', 'nincs', 10, 0, 2000, 1),
(125, 'Iszony', 'Németh László', '290851498489095', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2000, 1),
(126, 'Jadviga párnája', 'Závada Pál', '201233068800962', 'Budapest', 'Panem Kiadó', 'nincs', 13, 0, 2006, 1),
(127, 'Kard és kasza', 'Wass Albert', '899167285811739', 'Budapest', 'Panem Kiadó', 'nincs', 21, 0, 2000, 1),
(128, 'Karnevál', 'Hamvas Béla', '221476051576345', 'Budapest', 'Readers Digest', 'nincs', 10, 0, 2010, 1),
(129, 'Kincskeres? kisködmön', 'Móra Ferenc', '505738764466529', 'Budapest', 'Alexandra', 'nincs', 2, 0, 1980, 1),
(130, 'Különös házasság', 'Mikszáth Kálmán', '717328245013106', 'Budapest', 'Akadémiai Kiadó', 'nincs', 1, 0, 1985, 1),
(131, 'Légy jó mindhalálig', 'Móricz Zsigmond', '942390795787287', 'Budapest', 'Panem Kiadó', 'nincs', 6, 0, 2000, 1),
(132, 'Mesemaraton', 'Vavyan Fable', '796325586243467', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2000, 1),
(133, 'Ne féljetek', 'Jókai Anna', '382533843543762', 'Budapest', 'Panem Kiadó', 'nincs', 13, 0, 2004, 1),
(134, 'Pacsirta', 'Kosztolányi Dezs?', '94027796401,3181', 'Budapest', 'Readers Digest', 'nincs', 21, 0, 2000, 1),
(135, 'Pete Pite', 'Nógrádi Gábor', '57195551278131', 'Budapest', 'Panem Kiadó', 'nincs', 10, 0, 2000, 1),
(136, 'Piszkos Fred, a kapitány', 'Rejt? Jen?', '736876574564072', 'Budapest', 'Panem Kiadó', 'nincs', 2, 0, 2000, 1),
(137, 'Pokolbéli víg napjaim', 'Faludy György', '279223977302103', 'Budapest', 'Panem Kiadó', 'nincs', 13, 0, 2000, 1),
(138, 'Régimódi történet', 'Szabó Magda', '118633718138077', 'Budapest', 'Readers Digest', 'nincs', 21, 0, 2000, 1),
(139, 'Rokonok', 'Móricz Zsigmond', '228615516368545', 'Budapest', 'Alexandra', 'nincs', 10, 0, 2000, 1),
(140, 'Sorstalanság', 'Kertész Imre', '682715866556887', 'Budapest', 'Akadémiai Kiadó', 'nincs', 2, 0, 2000, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hystorybooks`
--

CREATE TABLE IF NOT EXISTS `hystorybooks` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `bookid` int(11) NOT NULL,
  `startdate` int(11) NOT NULL,
  `deadlinedate` int(11) NOT NULL,
  `enddate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `lentbooks`
--

CREATE TABLE IF NOT EXISTS `lentbooks` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `bookid` int(11) NOT NULL,
  `startdate` int(11) NOT NULL,
  `deadlinedate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- A tábla adatainak kiíratása `lentbooks`
--

INSERT INTO `lentbooks` (`id`, `userid`, `bookid`, `startdate`, `deadlinedate`) VALUES
(1, 34, 3, 20130506, 20130509),
(5, 12, 3, 20130723, 20130727);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `is_admin` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `address`, `is_admin`) VALUES
(1, 'Hullár Tamás', 's', 'a', '6421, Kisszállás Otthon u. 34.', 0),
(2, 'Nagy Bence', '111111', 'Nagy.Bence@gmail.com', '3554, Bükkaranyos Hajó u. 51.', 0),
(3, 'Péter Anna', '111111', 'Péter.Anna@gmail.com', '9547, Karakó Bem József u. 23.', 0),
(4, 'Vörös Krisztián', '111111', 'Vörös.Krisztián@gmail.com', '8447, Ajka Nefelejcs u. 3.', 0),
(5, 'Schäfer Andrea', '111111', 'Schäfer.Andrea@gmail.com', '3511, Miskolc Vöröshajnal út 75.', 0),
(6, 'Fehér Zoltán', '111111', 'Fehér.Zoltán@gmail.com', '9985, Fels?szölnök Harmadik u. 63.', 0),
(7, 'Novák Péter', '111111', 'Novák.Péter@gmail.com', '4320, Nagykálló Flórián út 24.', 0),
(8, 'Szemz? Roland', '111111', 'Szemz?.Roland@gmail.com', '2064, Bicske Esze Tamás u. 70.', 0),
(9, 'Krammer Martin', '111111', 'Krammer.Martin@gmail.com', '6031, Szentkirály Szöv? u. 88.', 0),
(10, 'Sétáló Szimóna', '111111', 'Sétáló.Szimóna@gmail.com', '8734, Somogyzsitfa Csutak Kálmán u. 25.', 0),
(11, 'Pass Nikolett', '111111', 'Pass.Nikolett@gmail.com', '4262, Nyíracsád Kiss Ern? u. 75.', 0),
(12, 'Csubák Ádám', '111111', 'Csubák.Ádám@gmail.com', '6785, Pusztamérges Tavasz u. 66.', 0),
(13, 'Vedelek Eszter', '111111', 'Vedelek.Eszter@gmail.com', '1163, Budapest XVI. Posta u. 65.', 0),
(14, 'Szabó Bettina', '111111', 'Szabó.Bettina@gmail.com', '8738, Nemesvid Szöv? sétány 26.', 0),
(15, 'Komlós Krisztina', '111111', 'Komlós.Krisztina@gmail.com', '6754, Újszentiván István király u. 49.', 0),
(16, 'Kupsza Réka', '111111', 'Kupsza.Réka@gmail.com', '2532, Tokodaltáró Kiss Ern? u. 29.', 0),
(17, 'L?kös Beatrix', '111111', 'L?kös.Beatrix@gmail.com', '5061, Tiszasüly Sorház u. 87.', 0),
(18, 'Tábori Nikolett', '111111', 'Tábori.Nikolett@gmail.com', '8724, Inke Tél u. 67.', 0),
(19, 'Szujer Lajos', '111111', 'Szujer.Lajos@gmail.com', '7304, Mánfa Vörössipkás út 66.', 0),
(20, 'Kocsis Benjámin', '111111', 'Kocsis.Benjámin@gmail.com', '4536, Ramocsaháza Juhar u. 56.', 0),
(21, 'Tóth Bettina', '111111', 'Tóth.Bettina@gmail.com', '8242, Balatonudvari Arany János u. 95.', 0),
(22, 'Bölcskei Sándor', '111111', 'Bölcskei.Sándor@gmail.com', '2677, Herencsény Szabó Ervin u. 82.', 0),
(23, 'Erdei Viktória', '111111', 'Erdei.Viktória@gmail.com', '8143, Sárszentmihály Szárcsa krt. 96.', 0),
(24, 'Gál Ibolya', '111111', 'Gál.Ibolya@gmail.com', '7381, Ág Traktoros u. 55.', 0),
(25, 'Bíró Judit', '111111', 'Bíró.Judit@gmail.com', '9123, Kajárpéc Munkás u. 18.', 0),
(26, 'Huber Attila', '111111', 'Huber.Attila@gmail.com', '8973, Alsószenterzsébet Váci Mihály u. 99.', 0),
(27, 'Mészáros Dorina', '111111', 'Mészáros.Dorina@gmail.com', '5836, Dombegyház Harmat u. 59.', 0),
(28, 'Kövesdi Renátó', '111111', 'Kövesdi.Renátó@gmail.com', '3647, Csokvaomány Harmat út 96.', 0),
(29, 'Váczi Lilla', '111111', 'Váczi.Lilla@gmail.com', '2170, Aszód Virág Benedek krt. 31.', 0),
(30, 'Végh Flóra', '111111', 'Végh.Flóra@gmail.com', '3501, Miskolc Csáky László u. 37.', 0),
(31, 'Francsovics Dániel', '111111', 'Francsovics.Dániel@gmail.com', '9776, Püspökmolnári Török Bálint u. 69.', 0),
(32, 'Fischer Em?ke', '111111', 'Fischer.Em?ke@gmail.com', '6348, Érsekhalma Tél u. 9.', 0),
(33, 'Novoszath Szilvia', '111111', 'Novoszath.Szilvia@gmail.com', '1213, Budapest XXI. Tóth Kálmán u. 24.', 0),
(34, 'Kiss Ádám', '111111', 'Kiss.Ádám@gmail.com', '9812, Telekes Kis tér 11.', 0),
(35, 'Rácz Bianka', '111111', 'Rácz.Bianka@gmail.com', '3127, Kazár Nagyatádi u. 67.', 0),
(36, 'Végh Laura', '111111', 'Végh.Laura@gmail.com', '4183, Kaba Muskátli u. 92.', 0),
(37, 'Mihó Péter', '111111', 'Mihó.Péter@gmail.com', '5650, Mez?berény Bem József u. 39.', 0),
(38, 'Szombati József', '111111', 'Szombati.József@gmail.com', '5476, Szelevény Kálmán király u. 34.', 0),
(39, 'Végel Melinda', '111111', 'Végel.Melinda@gmail.com', '2911, Mocsa Liliom u. 50.', 0),
(40, 'Csáki Dorina', '111111', 'Csáki.Dorina@gmail.com', '3658, Borsodbóta Rét u. 38.', 0),
(41, 'Mayer Szimonetta', '111111', 'Mayer.Szimonetta@gmail.com', '7394, Bodolyabér Csinos u. 50.', 0),
(42, 'Karagity Róbert', '111111', 'Karagity.Róbert@gmail.com', '4843, Hetefejércse Gárdonyi u. 32.', 0),
(43, 'Jámbor Viktória', '111111', 'Jámbor.Viktória@gmail.com', '7171, Sióagárd Csinos u. 53.', 0),
(44, 'Bohos Norbert', '111111', 'Bohos.Norbert@gmail.com', '9172, Gy?rzámoly Esze Tamás u. 4.', 0),
(45, 'Fels? Roland', '111111', 'Fels?.Roland@gmail.com', '3852, Hernádszentandrás Csáky László u. 7.', 0),
(46, 'Felnagy Gabriella', '111111', 'Felnagy.Gabriella@gmail.com', '1096, Budapest IX. Fuvaros út 59.', 0),
(47, 'Dunay Mihály', '111111', 'Dunay.Mihály@gmail.com', '8739, Nagyszakácsi Lovarda u. 67.', 0),
(48, 'Gyúró Lajos', '111111', 'Gyúró.Lajos@gmail.com', '4225, Debrecen Tükör u. 75.', 0),
(49, 'Petrity Anett', '111111', 'Petrity.Anett@gmail.com', '6430, Bácsalmás Harmat u. 44.', 0),
(50, 'Burány Dóra', '111111', 'Burány.Dóra@gmail.com', '7775, Illocska Könyök u. 65.', 0),
(51, 'Óvári Tamás', '111111', 'Óvári.Tamás@gmail.com', '5726, Méhkerék Attila út 16.', 0),
(52, 'Északi Domonkos', '111111', 'Északi.Domonkos@gmail.com', '3837, Alsógagy Szabadi u. 72.', 0),
(53, 'Sz?ke Petra', '111111', 'Sz?ke.Petra@gmail.com', '7471, Zimány Kálmán király u. 34.', 0),
(54, 'Tokodi Gréta', '111111', 'Tokodi.Gréta@gmail.com', '5900, Orosháza Íjász sétány 20.', 0),
(55, 'Kelemen András', '111111', 'Kelemen.András@gmail.com', '3412, Bogács Ady Endre út 27.', 0),
(56, 'Szombati Attila', '111111', 'Szombati.Attila@gmail.com', '4320, Nagykálló Szél u. 66.', 0),
(57, 'Gill Eszter', '111111', 'Gill.Eszter@gmail.com', '3794, Boldva Bástya u. 101.', 0),
(58, 'Barta Károly', '111111', 'Barta.Károly@gmail.com', '6787, Zákányszék Pattantyús u. 26.', 0),
(59, 'Gregor Péter', '111111', 'Gregor.Péter@gmail.com', '7191, H?gyész Süt? u. 3.', 0),
(60, 'Gelányi Ádám', '111111', 'Gelányi.Ádám@gmail.com', '3352, Feldebr? Mohács tér 30.', 0),
(61, 'Horváth Ádám', '111111', 'Horváth.Ádám@gmail.com', '2834, Tardos Röppenty? u. 83.', 0),
(62, 'Kovács Gerg?', '111111', 'Kovács.Gerg?@gmail.com', '2065, Mány Magyar u. 35.', 0),
(63, 'Suhajda Rebeka', '111111', 'Suhajda.Rebeka@gmail.com', '3732, Kurityán Kisfaludy u. 48.', 0),
(64, 'Csernovics Anikó', '111111', 'Csernovics.Anikó@gmail.com', '3904, Legyesbénye Kis u. 96.', 0),
(65, 'L?kös Bíborka', '111111', 'L?kös.Bíborka@gmail.com', '2215, Káva Lovas u. 44.', 0),
(66, 'Horváth Ágota', '111111', 'Horváth.Ágota@gmail.com', '9684, Egervölgy Vörössipkás u. 28.', 0),
(67, 'Járai András', '111111', 'Járai.András@gmail.com', '9512, Ostffyasszonyfa Erd? u. 46.', 0),
(68, 'Jakubecz Dénes', '111111', 'Jakubecz.Dénes@gmail.com', '9774, Gyanógeregye Gárdonyi u. 17.', 0),
(69, 'Illés Bence', '111111', 'Illés.Bence@gmail.com', '3656, Sajómercse Battyhány u. 29.', 0),
(70, 'Admin Józsi', '666666', 'Admin.Józsi', '5600,BékéscsabaAdmin u 666.', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
