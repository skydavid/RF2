<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
	String email=(String)session.getAttribute("email");
	if(email==null){
		response.sendRedirect("index.jsp");
	}
    %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style.css" type="text/css"/>
<title>new book</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	
		
	<div id="content">
		<form action="konyv_hozzaadasa.jsp" method="post">
			<table class="center">
				<tr>
					<td>
						Cím:
					</td>
					<td>
						<input type="text" name="title"/>
					</td>
				</tr>
				<tr>
					<td>
						Szerző:
					</td>
					<td>
						<input type="text" name="author"/>
					</td>
				</tr>
				<tr>
					<td>
						IMEI:
					</td>
					<td>
						<input type="number" min="1" name="imei"/>
					</td>
				</tr>
				<tr>
					<td>
						Kiadó:
					</td>
					<td>
						<input type="text" name="publisher"/>
					</td>
				</tr>
				<tr>
					<td>
						Műfaj:
					</td>
					<td>
						<input type="text" name="genre"/>
					</td>
				</tr>
				<tr>
					<td>
						Kiadás dátuma:
					</td>
					<td>
						<input type="number" min="1" name="date"/>
					</td>
				</tr>
				<tr>
					<td>
						Darabszám:
					</td>
					<td>
						<input type="number" min="1" name="piece"/>
					</td>
				</tr>
				<tr>
					<td>
						Leírás:
					</td>
					<td>
						<textarea rows="4" cols="20" name="description"></textarea>
					</td>
				</tr>
				<tr>
					<td>
						Kép linkje:
					</td>
					<td>
						<input type="text" name="image"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" value="Létrehozás"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>