<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="library.*"%>
    <%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<%
String email=(String)session.getAttribute("email");
String role=(String)session.getAttribute("role");
if(email==null){
	response.sendRedirect("index.jsp");
}







String id = request.getParameter("userId");
String driverName = "com.mysql.jdbc.Driver";
String connectionUrl = "jdbc:mysql://localhost:3306/";
String dbName = "library";
String userId = "root";
String password = "";

try {
Class.forName(driverName);
} catch (ClassNotFoundException e) {
e.printStackTrace();
}

Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>       


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style.css" type="text/css"/>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"/>
<title>konyvek</title>
</head>
<body>
<jsp:include page="header.jsp" />

<div class="new-book"><a href="uj_konyv.jsp">Új könyv létrehozása</a></div>
<div id="content" >
<table id="lista" align="center" cellpadding="5" cellspacing="5" border="1" bgcolor="#808080">
<tr>

</tr>
<tr bgcolor="#003366">
<td ><b>Borító</b></td>
<td ><b>Szerző</b></td>
<td ><b>Cím</b></td>
<td ><b>IMEI</b></td>
<td ><b>Műfaj</b></td>
<td ><b>Kiadó</b></td>
<td><b>Kiadás ideje</b></td>
<td ><b>Leírás</b></td>
<td ><b>Kölcsönözhető (db)</b></td>
<%if (!role.equals("USER")) { %>
<td ><b>Kölcsönzések száma</b></td>
<% } %>
<td ><b>comment</b></td>



</tr>
<%
try{ 
connection = DriverManager.getConnection(connectionUrl+dbName, userId, password);
statement=connection.createStatement();
String sql ="SELECT * FROM books";

resultSet = statement.executeQuery(sql);
while(resultSet.next()){
%>
<tr bgcolor="#003366">
<td><img src="<%=resultSet.getString("imageurl") %>" alt="nincs kep" height="100" width="100"></td>
<td><%=resultSet.getString("author") %></td>
<td><%=resultSet.getString("title") %></td>
<td><%=resultSet.getString("imei") %></td>
<td><%=resultSet.getString("genre") %></td>
<td><%=resultSet.getString("publisherName") %></td>
<td><%=resultSet.getString("publishingDate") %></td>
<td><%=resultSet.getString("description") %></td>
<td><%=resultSet.getInt("availablePieces") %></td>
<%if (!role.equals("USER")) { %>
<td><%=resultSet.getString("numberOfLoanings") %></td>
<% } %>
<td>
<form method="post" action="comment.jsp">
	<input type="submit" value="commentek megjelenitese"/>
	<input value="<%=resultSet.getInt("id") %>" name="commentbid" type="hidden" value="commentek"/>
</form>
</td>
<td>
<form method="post" action="kolcsonzes.jsp">
	<input type="submit" value="Kölcsönzés"/>
	<input value="<%=resultSet.getInt("id") %>" name="bookId" type="hidden" value="Kölcsönzés"/>
</form>
</td>
<%if (!role.equals("USER")) { %>
<td>
	<form method="post" action="konyv_szerkesztese.jsp">
		<button type="submit" class="btn-link"><i class="icon-edit-sign icon-large"></i></button>
		<input value="<%=resultSet.getInt("id") %>" name="bookId" type="hidden" value="Szerkesztés"/>
	</form>
</td>
<td>
	<form method="post" action="konyv_torles.jsp">
		<button type="submit" class="btn-link"><i class="icon-remove-sign icon-large"></i></button>
		<input value="<%=resultSet.getInt("id") %>" name="bookId" type="hidden" value="Törlés"/>
	</form>
</td>
<% } %>

</tr>

<% 
}

} catch (Exception e) {
e.printStackTrace();
}
%>
</table>
		
		
			
	</div>

</body>
</html>