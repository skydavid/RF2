<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="library.*"%>
    <%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>

<%
String email=(String)session.getAttribute("email");
String role=(String)session.getAttribute("role");
if(email==null){
	response.sendRedirect("index.jsp");
}





String id = request.getParameter("userId");
String driverName = "com.mysql.jdbc.Driver";
String connectionUrl = "jdbc:mysql://localhost:3306/";
String dbName = "library";
String userId = "root";
String password = "";

try {
Class.forName(driverName);
} catch (ClassNotFoundException e) {
e.printStackTrace();
}

Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>       


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style.css" type="text/css"/>
<title>konyvek</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	
	<div id="content" >
<table id="lista" align="center" cellpadding="5" cellspacing="5" border="1" bgcolor="#808080">
<tr>

</tr>
<tr bgcolor="#003366">
<td ><b>Szerző</b></td>
<td ><b>Cím</b></td>
<td ><b>Felhasználó</b></td>
<td ><b>Kölcsönzés ideje</b></td>
<td ><b>Lejárati idő</b></td>
<td ><b>Visszahozva</b></td>



</tr>
<%
try{ 
connection = DriverManager.getConnection(connectionUrl+dbName, userId, password);
statement=connection.createStatement();
String sql ="SELECT books.author, books.title, users.name, lentbooks.startDate, lentbooks.deadlineDate, lentbooks.returnDate, lentbooks.id from lentbooks LEFT JOIN books ON books.id=lentbooks.bookid LEFT JOIN users ON users.id=lentbooks.userid";

resultSet = statement.executeQuery(sql);
while(resultSet.next()){
%>
<tr bgcolor="#003366">

<td><%=resultSet.getString("author") %></td>
<td><%=resultSet.getString("title") %></td>
<td><%=resultSet.getString("name") %></td>
<td><%=resultSet.getString("startDate") %></td>
<td><%=resultSet.getString("deadlineDate") %></td>
<td><%=resultSet.getString("returnDate") != null ? resultSet.getString("returnDate") : "Nincs visszahozva" %></td>
<%if (!role.equals("USER") && resultSet.getString("returnDate") == null) { %>
<td>
<form method="post" action="visszahozas.jsp">
	<input type="submit" value="Visszahozva"/>
	<input value="<%=resultSet.getInt("id") %>" name="id" type="hidden"/>
</form>
</td>
<% } %>
</tr>

<% 
}

} catch (Exception e) {
e.printStackTrace();
} finally {
	if (statement != null) {
		statement.close();
	}
	if (connection != null) {
		connection.close();
	}
}
%>
</table>
</div>
	<jsp:include page="footer.jsp" />
</body>
</html>