<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
    
<%
String email=(String)session.getAttribute("email");
if(email==null){
	response.sendRedirect("index.jsp");
}
String role=(String)session.getAttribute("role");
if (role.equals("USER")) { 
	response.sendRedirect("konyvek.jsp");
}
String id = request.getParameter("id");
String driverName = "com.mysql.jdbc.Driver";
String connectionUrl = "jdbc:mysql://localhost:3306/";
String dbName = "library";
String userId = "root";
String password = "";

try {
Class.forName(driverName);
} catch (ClassNotFoundException e) {
e.printStackTrace();
}

Connection connection = null;
PreparedStatement ps = null;

try{ 
	connection = DriverManager.getConnection(connectionUrl+dbName, userId, password);
	String updateTableSQL = "UPDATE lentbooks SET returnDate = ? WHERE id = ?";
	ps = connection.prepareStatement(updateTableSQL);
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date = new Date();
	ps.setString(1, dateFormat.format(date));
	ps.setString(2, id);
	ps.executeUpdate();
} catch (Exception e) {
	e.printStackTrace();
} finally {
	if (ps != null) {
		ps.close();
	}
	if (connection != null) {
		connection.close();
	}
}

response.sendRedirect("kolcsonzott.jsp");
%>