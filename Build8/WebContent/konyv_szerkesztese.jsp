<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<%
	String email=(String)session.getAttribute("email");
	out.println(email);
	if(email==null){
		response.sendRedirect("index.jsp");
	}
	String role=(String)session.getAttribute("role");
	if (role.equals("USER")) { 
		response.sendRedirect("konyvek.jsp");
	}
	String bookid = request.getParameter("bookId");
	String driverName = "com.mysql.jdbc.Driver";
	String connectionUrl = "jdbc:mysql://localhost:3306/";
	String dbName = "library";
	String userId = "root";
	String password = "";
	
	try {
	Class.forName(driverName);
	} catch (ClassNotFoundException e) {
	e.printStackTrace();
	}
	
	Connection connection = null;
	Statement st = null;
	ResultSet rs = null;
	try{ 
		connection = DriverManager.getConnection(connectionUrl+dbName, userId, password);
		String sql = "SELECT * FROM books WHERE id = " + bookid;
		st = connection.createStatement();
		rs = st.executeQuery(sql);
		
		while (rs.next()) {
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style.css" type="text/css"/>
<title>new book</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	
		
	<div id="content">
		<form action="szerkesztes.jsp" method="post">
			<table class="center">
				<tr>
					<td>
						Cím:
					</td>
					<td>
						<input type="text" name="title" value="<%=rs.getString("title")%>"/>
					</td>
				</tr>
				<tr>
					<td>
						Szerző:
					</td>
					<td>
						<input type="text" name="author" value="<%=rs.getString("author")%>"/>
					</td>
				</tr>
				<tr>
					<td>
						IMEI:
					</td>
					<td>
						<input type="number" min="1" name="imei" value="<%=rs.getString("imei")%>"/>
					</td>
				</tr>
				<tr>
					<td>
						Kiadó:
					</td>
					<td>
						<input type="text" name="publisher" value="<%=rs.getString("publisherName")%>"/>
					</td>
				</tr>
				<tr>
					<td>
						Műfaj:
					</td>
					<td>
						<input type="text" name="genre" value="<%=rs.getString("genre")%>"/>
					</td>
				</tr>
				<tr>
					<td>
						Kiadás dátuma:
					</td>
					<td>
						<input type="number" min="1" name="date" value="<%=rs.getInt("publishingDate")%>"/>
					</td>
				</tr>
				<tr>
					<td>
						Darabszám:
					</td>
					<td>
						<input type="number" min="1" name="piece" value="<%=rs.getString("piece")%>"/>
					</td>
				</tr>
				<tr>
					<td>
						Leírás:
					</td>
					<td>
						<textarea rows="4" cols="20" name="description"><%=rs.getString("description")%></textarea>
					</td>
				</tr>
				<tr>
					<td>
						Kép linkje:
					</td>
					<td>
						<input type="text" name="image" value="<%=rs.getString("imageurl")%>"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" value="Szerkesztés"/>						
						<input value="<%=rs.getString("id") %>" name="bookId" type="hidden" value="Kölcsönzés"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>
<%
		}
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		if (st != null) {
			st.close();
		}
		if (connection != null) {
			connection.close();
		}
	}
%>