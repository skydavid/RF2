<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.*"%>

<%
String role=(String)session.getAttribute("role");
if (role.equals("USER")) { 
	response.sendRedirect("konyvek.jsp");
}
String bookid = request.getParameter("bookId");
String title = request.getParameter("title");
String author = request.getParameter("author");
String imei = request.getParameter("imei");
String publisher = request.getParameter("publisher");
String genre = request.getParameter("genre");
String image = request.getParameter("image");
String date = request.getParameter("date");
String piece = request.getParameter("piece");
String description = request.getParameter("description");

	String driverName = "com.mysql.jdbc.Driver";
	String connectionUrl = "jdbc:mysql://localhost:3306/";
	String dbName = "library";
	String userId = "root";
	String password = "";
	
	try {
	Class.forName(driverName);
	} catch (ClassNotFoundException e) {
	e.printStackTrace();
	}
	
	Connection connection = null;
	PreparedStatement ps = null;
	try{ 
		connection = DriverManager.getConnection(connectionUrl+dbName, userId, password);
		String updateTableSQL = "INSERT INTO books (author, title, imei, publisherName, publishingDate, description, genre, piece, availablePieces, numberOfLoanings, imageurl) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		ps = connection.prepareStatement(updateTableSQL);
		ps.setString(1, author);
		ps.setString(2, title);
		ps.setString(3, imei);
		ps.setString(4, publisher);
		ps.setInt(5, Integer.parseInt(date));
		ps.setString(6, description);
		ps.setString(7, genre);
		ps.setInt(8, Integer.parseInt(piece));
		ps.setInt(9, Integer.parseInt(piece));
		ps.setInt(10, 0);
		ps.setString(11, image);
		ps.executeUpdate();
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		if (ps != null) {
			ps.close();
		}
		if (connection != null) {
			connection.close();
		}
	}
%>