<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div id="header">
	<h1>Könyves Kálmán Online Könyvtár</h1>
	<nav>
		<a href="home.jsp">Kezdőlap</a>
	
		<a href="kolcsonzott.jsp">Kölcsönzött</a>
	
		<a href="konyvek.jsp">Könyvek</a>
	
		<a href="profil.jsp">Profil</a>
	
		<a href="logout.jsp">Kilépés</a>				
		
		<p>Aktuális felhasználó: 
		<% 
		String email=(String)session.getAttribute("email");
		out.println(email);
		%>
		</p>
		<table>
		<tr>
		<td>
		<form method="post" action="kereso.jsp">
			<a>Keresés: </a><input  type="text" name="pid" id="pid"> <input  type="submit" name="submit" value="kereső">
		</form>
		</td>
		<td>
		</td>
		<td>
		<form action="lista.jsp">
	  		<select name="item">
	  			<option value="" disabled selected>Szűrés műfajra</option>
	   			<option value="scifi">scifi</option>
	    		<option value="kotelezo">kötelező</option>
	    		<option value="thriller">thriller</option>
	    		<option value="horror">horror</option>
	  		</select>
	  		<input type="submit" value="szűrés">
		</form>
		</td>
		</tr>
		</table>
	</nav>
</div>