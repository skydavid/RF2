<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
   
    <%@page import="library.*"%>    
   
    <jsp:useBean id="obj" class="library.User"/>

<jsp:setProperty property="*" name="obj"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
String email=(String)session.getAttribute("email");
if(email==null){
	response.sendRedirect("index.jsp");
}


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style.css" type="text/css"/>
<title>home</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	
	<jsp:include page="footer.jsp" />
</body>
</html>