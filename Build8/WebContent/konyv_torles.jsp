<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.sql.DriverManager"%>
	<%@page import="java.sql.ResultSet"%>
	<%@page import="java.sql.PreparedStatement"%>
	<%@page import="java.sql.Connection"%>

<%
String email=(String)session.getAttribute("email");
out.println(email);
if(email==null){
	response.sendRedirect("index.jsp");
}
String role=(String)session.getAttribute("role");
if (role.equals("USER")) { 
	response.sendRedirect("konyvek.jsp");
}
String bookid = request.getParameter("bookId");
String driverName = "com.mysql.jdbc.Driver";
String connectionUrl = "jdbc:mysql://localhost:3306/";
String dbName = "library";
String userId = "root";
String password = "";

try {
	Class.forName(driverName);
} catch (ClassNotFoundException e) {
	e.printStackTrace();
}

Connection connection = null;
PreparedStatement ps = null;
ResultSet resultSet = null;
try{ 
	connection = DriverManager.getConnection(connectionUrl+dbName, userId, password);
	String sql ="DELETE FROM books WHERE id = ?";
	ps=connection.prepareStatement(sql);
	ps.setString(1, bookid);
	ps.executeUpdate();
} catch (Exception e) {
	e.printStackTrace();
} finally {
	if (ps != null) {
		ps.close();
	}
	if (connection != null) {
		connection.close();
	}
}

response.sendRedirect("konyvek.jsp");
%>